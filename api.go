package socbot

import (
	"bytes"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/zanven/socbot/op"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

type SocBot interface {
	Start(token string) error
	Close()
}

// New Returns a new SocBot
func New(OpChannelID string, guildID string, botName string, dev bool) SocBot {
	return &socBot{OpChannelID: OpChannelID, GuildID: guildID, BotName: botName, devMode: dev}
}

type socBot struct {
	dg          *discordgo.Session
	onClose     chan struct{}
	OpChannelID string
	GuildID     string
	BotName     string
	devMode     bool
}

func (s *socBot) Start(token string) (err error) {
	s.dg, err = discordgo.New("Bot " + token)
	s.dg.AddHandler(s.messageCreateHandler)
	err = s.dg.Open()
	if err != nil {
		return err
	}
	s.onClose = make(chan struct{}, 1)

	return err
}

func (s *socBot) Close() {
	if s.dg != nil {
		s.dg.Close()
		close(s.onClose)
	}
}

func (s *socBot) UpdateLoop() {
	// loop setup here
	s.housekeeping()
	for {
		select {
		case <-s.onClose:
			return

		case <-time.After(time.Hour):
			s.housekeeping()

		}
	}
}

// go t:hrough the op channel and remove all unwanted messages and old ops.
func (s *socBot) housekeeping() {
	//TODO: Cleanup the thingos

}

func (s *socBot) messageCreateHandler(sess *discordgo.Session, msg *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if msg.Author.ID == sess.State.User.ID || (msg.GuildID != s.GuildID && msg.GuildID != "") {
		return
	}

	// well if it isn't for us fuck it
	if strings.HasPrefix(strings.ToLower(msg.Content), s.BotName) == false {
		return
	}
	responseChan := s.OpChannelID
	if s.devMode  || msg.GuildID == ""{
		responseChan = msg.ChannelID
	}

	//TODO: Workout how to clean this crap up :/
    app := kingpin.New(s.BotName, "socomd Helper Bot\n\nExample usage:\n\tCreate a mini op in 20 minutes: (help function /socbot op create --help)\n\t\t/socbot op create -d 20m")
	var buf bytes.Buffer
	app.Writer(&buf)
	app.UsageWriter(&buf)
	app.Terminate(func(int) {
		// sess.ChannelMessageSend(msg.ChannelID, buf.String())
		return
	})
	app.UsageTemplate(kingpin.CompactUsageTemplate)
	args, err := convertToDummyArgs(msg.Content)
	if err != nil {
		sess.ChannelMessageSend(msg.ChannelID, fmt.Sprintf("error: %s", err))
		return
	}
	app.Command("ping", "Ping SocBot").Action(func(*kingpin.ParseContext) error {
		sess.ChannelMessageSend(msg.ChannelID, "Pong!")
		return nil
	})
	op.AddOpCommands(app, sess, msg, responseChan)

	_, err = app.Parse(args[1:])
	if err != nil {
		var usageBuf bytes.Buffer
		app.UsageWriter(&usageBuf)
		app.Usage(args[1:])
		sess.ChannelMessageSend(msg.ChannelID, fmt.Sprintf("`%s`\n```%s```", err, usageBuf.String()))
		return
	}
	return
}

//https://stackoverflow.com/questions/34118732/parse-a-command-line-string-into-flags-and-arguments-in-golang
func convertToDummyArgs(command string) ([]string, error) {
	var args []string
	state := "start"
	current := ""
	quote := "\""
	escapeNext := true
	for i := 0; i < len(command); i++ {
		c := command[i]

		if state == "quotes" {
			if string(c) != quote {
				current += string(c)
			} else {
				args = append(args, current)
				current = ""
				state = "start"
			}
			continue
		}

		if escapeNext {
			current += string(c)
			escapeNext = false
			continue
		}

		if c == '\\' {
			escapeNext = true
			continue
		}

		if c == '"' || c == '\'' {
			state = "quotes"
			quote = string(c)
			continue
		}

		if state == "arg" {
			if c == ' ' || c == '\t' {
				args = append(args, current)
				current = ""
				state = "start"
			} else {
				current += string(c)
			}
			continue
		}

		if c != ' ' && c != '\t' {
			state = "arg"
			current += string(c)
		}
	}

	if state == "quotes" {
		return []string{}, errors.New(fmt.Sprintf("Unclosed quote in command line: %s", command))
	}

	if current != "" {
		args = append(args, current)
	}

	return args, nil
}
