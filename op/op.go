package op

import (
	"bytes"
	"fmt"
	"time"

	"github.com/alecthomas/template"
	"github.com/bwmarrin/discordgo"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

func AddOpCommands(app *kingpin.Application, sess *discordgo.Session, msg *discordgo.MessageCreate, responseChannel string) {
	create := &opCreateCommand{sess: sess, msg: msg, responseChannel: responseChannel}

    op := app.Command("op", "Create, edit & delete miniop events\n\nCreate Op Help:\t/socbot op create --help\n")
	createCommand := op.Command("create", "create a miniop event examples:\n\t/socbot op create -d 2h20m\n\t/socbot op create -d \"22:30 25/02/19 AEDT\" -z Zanven -n \"My Cool Op\"\n\t/socbot op create -d \"22:30 25\" -i \"super useful op info\" -m Core -m Alibad-region\n").Action(create.run)
	// createCommand.Flag("time", "time of the event, format: 09:00pm").Required().Short('t').StringVar(&create.time)
	createCommand.Flag("date", "date of op, examples: HH:MM dd/mm/yy - 40m - 2h45m").Required().Short('d').StringVar(&create.Date)
	createCommand.Flag("name", "Name of the miniop").Short('n').StringVar(&create.Name)
	createCommand.Flag("info", "information about the op would be nice ye?").Short('i').Default("no info, you work it out").StringVar(&create.Info)
	createCommand.Flag("mods", "mods needed for the operation").Short('m').Default("Core").StringsVar(&create.Mods)
	createCommand.Flag("server", "Which server is being used").Short('s').Default("Secondary").StringVar(&create.Server)
	createCommand.Flag("zeus", "Who are the zeus's").Short('z').StringVar(&create.Zeus)

	// Need a delete command

	// Need a edit command that delete's then new's

	return
}

const (
	opTextTemplate string = "@here {{.Name}} MiniOP\n```Date: {{.Date}}\nZeus: {{.Zeus}}\nCreator: {{.Creator}}\nServer: {{.Server}}\nMods:{{range $i, $x := .Mods}}\n\t{{$x}}{{end}}\n\nInfo:\n\t{{.Info}}```"
)

type opCreateCommand struct {
	Date    string
	Name    string
	Mods    []string
	Info    string
	Server  string
	Zeus    string
	Creator string

	sess            *discordgo.Session
	msg             *discordgo.MessageCreate
	responseChannel string
}

func (occ *opCreateCommand) run(*kingpin.ParseContext) error {
	var err error
	occ.Date, err = timeValidator(occ.Date)
	if err != nil {
		return err
	}
	occ.Creator = occ.msg.Author.Username
	if occ.Zeus == "" {
		occ.Zeus = occ.Creator
	}
	tmpl, err := template.New("").Parse(opTextTemplate)
	if err != nil {
		occ.sess.ChannelMessageSend(occ.msg.ChannelID, fmt.Sprintf("Tell Zan and close your eyes tc1: ```%s```", err))
		return nil
	}
	var buf bytes.Buffer
	err = tmpl.Execute(&buf, occ)
	if err != nil {
        occ.sess.ChannelMessageSend(occ.msg.ChannelID, fmt.Sprintf("Tell Zan and close your eyes tc2: ```%s```", err))
		return err
	}
	msg ,err := occ.sess.ChannelMessageSend(occ.responseChannel, buf.String())
    if err != nil {
        return err
    }
    occ.sess.MessageReactionAdd(occ.responseChannel,msg.ID,"👍")
    occ.sess.MessageReactionAdd(occ.responseChannel,msg.ID,"👎")
	return nil
}

func timeValidator(value string) (string, error) {

	// zone requires seconds
	zone := time.FixedZone("AEST", 10*60*60)
	// validate day, presume long format first - below is default format schema
	// Mon Jan 2 15:04:05 -0700 MST 2006
	tNow := time.Now().In(zone)

	var finalErr string
	var t time.Time
	// Try Longest version
	// HH:MM DD/MM/YY MST
	t, err := time.Parse("15:04 02/01/06 MST", value)
	if err == nil {
		t = t.In(zone)
		return t.Format("15:04 02/01/06 MST"), nil
	}
	finalErr += fmt.Sprintf("\n%s", err)
	// HH:MM DD/MM/YY
	t, err = time.ParseInLocation("15:04 02/01/06", value, zone)
	if err == nil {
		t = time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), 0, 0, zone)
		// now check for a local increment time
		return t.Format("15:04 02/01/06 MST"), nil
	}
	finalErr += fmt.Sprintf("\n%s", err)

	// HH:MM DD/MM MST
	t, err = time.Parse("15:04 02/01 MST", value)
	if err == nil {
		t = t.In(zone)
		t = time.Date(tNow.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), 0, 0, zone)
		return t.Format("15:04 02/01/06 MST"), nil
	}
	finalErr += fmt.Sprintf("\n%s", err)

	// HH:MM DD/MM
	t, err = time.ParseInLocation("15:04 02/01", value, zone)
	if err == nil {
		t = time.Date(tNow.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), 0, 0, zone)
		return t.Format("15:04 02/01/06 MST"), nil
	}
	finalErr += fmt.Sprintf("\n%s", err)

	// HH:MM DD MST
	t, err = time.Parse("15:04 02 MST", value)
	if err == nil {
		t = t.In(zone)
		t = time.Date(tNow.Year(), tNow.Month(), t.Day(), t.Hour(), t.Minute(), 0, 0, zone)
		return t.Format("15:04 02/01/06 MST"), nil
	}
	finalErr += fmt.Sprintf("\n%s", err)

	// HH:MM DD
	t, err = time.ParseInLocation("15:04 02", value, zone)
	if err == nil {
		t = time.Date(tNow.Year(), tNow.Month(), t.Day(), t.Hour(), t.Minute(), 0, 0, zone)
		return t.Format("15:04 02/01/06 MST"), nil
	}
	finalErr += fmt.Sprintf("\n%s", err)

	// HH:MM MST
	t, err = time.Parse("15:04 MST", value)
	if err == nil {
		t = t.In(zone)
		t = time.Date(tNow.Year(), tNow.Month(), tNow.Day(), t.Hour(), t.Minute(), 0, 0, zone)
		return t.Format("15:04 02/01/06 MST"), nil
	}
	finalErr += fmt.Sprintf("\n%s", err)

	// HH:MM
	t, err = time.ParseInLocation("15:04", value, zone)
	if err == nil {
		t = time.Date(tNow.Year(), tNow.Month(), tNow.Day(), t.Hour(), t.Minute(), 0, 0, zone)
		return t.Format("15:04 02/01/06 MST"), nil
	}
	finalErr += fmt.Sprintf("\n%s", err)

	dur, err := time.ParseDuration(value)
	if err == nil {
		t = tNow.Add(dur)
		return t.Format("15:04 02/01/06 MST"), nil
	}

	finalErr += fmt.Sprintf("\n%s", err)

	return "", fmt.Errorf("%s", finalErr)
}
