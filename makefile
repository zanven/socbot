OS = $(shell uname -s)
ARCH = $(shell uname -m)
# setup makes sure your dev environment is good to go
.PHONY: setup
setup:
ifeq (,$(shell command -v docker 2> /dev/null))
	@echo "Installing Docker"
	curl https://get.docker.com/ | sh
	@echo "Docker Installed"
endif
ifeq (,$(shell command -v go 2> /dev/null))
	@echo "installing Golang"
	curl https://github.com/mohae/install-go/blob/master/install_go | sh
	@echo "Golang installed"
endif

# Run locally with a local token to do quick testing
.PHONY: run
run:
	GO111MODULE=on go run ./cmd/main.go -t $(shell cat ./token) -d

# auth url to get discords signed up to the bot with the correct permission scoping
auth:
	https://discordapp.com/api/oauth2/authorize?client_id=547248739295690773&scope=bot&permissions=207936

