module gitlab.com/zanven/socbot

require (
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf // indirect
	github.com/bwmarrin/discordgo v0.19.0
	github.com/gorilla/websocket v1.4.0
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/zanven42/argparse v0.0.0-20190221104856-659ef870d341
	golang.org/x/crypto v0.0.0-20190211182817-74369b46fc67
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
