from golang as builder

run mkdir /compile
WORKDIR /compile
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GO111MODULE=on go build -v -o /socbot cmd/main.go

# setup final container
FROM alpine
COPY --from=builder /socbot /socbot
RUN chmod +x /socbot
RUN apk add --no-cache ca-certificates
CMD ["/socbot"]
