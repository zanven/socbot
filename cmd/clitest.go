package main

import (
	"bytes"
	"fmt"
	"os"

	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

var ()

func main() {
	app := kingpin.New("clitest", "app helper text")
	_ = app.Command("A", "a help")

	var buf bytes.Buffer
	app.Writer(&buf)
	app.Terminate(func(int) {
		fmt.Println("errored:", buf.String())
		return
	})
	fmt.Println(os.Args[1:])
	finalcmd, err := app.Parse(os.Args[1:])
	if err != nil {
		fmt.Println("Err out:", err)
		fmt.Println(buf.String())
		fmt.Println(finalcmd)
		return
	}
	fmt.Println("final", finalcmd)

	return
}
