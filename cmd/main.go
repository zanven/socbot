package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/zanven/socbot"
)

// Variables used for command line parameters
var (
	Token string
	Debug bool
	// OpchannelID string
)

const (
	OpChannelID string = "400126404697653249" // MINIOPS-Channel
	GuildID     string = "389256100379230209" // SOCOMD
)

func init() {
	flag.StringVar(&Token, "t", "", "Bot Token")
	flag.BoolVar(&Debug, "d", false, "Debug mode")
	flag.Parse()
}

func main() {
	if Token == "" {
		Token = os.Getenv("TOKEN")
	}
	// Create a new Discord session using the provided bot token.
	botname := "/socbot"
	dev := false
	if Debug {
		botname = "/devsocbot"
		dev = true
	}
	sb := socbot.New(OpChannelID, GuildID, botname, dev)
	err := sb.Start(Token)
	if err != nil {
		fmt.Println("Failed to start server:", err)
		return
	}
	fmt.Println("Bot is now running.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	sb.Close()

}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}

	// if m.ChannelID == OpChannelID {
	// 	s.ChannelMessageDelete(m.ChannelID, m.ID)
	// }
	// If the message is "ping" reply with "Pong!"
	if m.Content == "/socbot ping" {
		s.ChannelMessageSend(m.ChannelID, "Pong!")
	}

	// If the message is "pong" reply with "Ping!"
	if m.Content == "pong" {
		s.ChannelMessageSend(m.ChannelID, "Ping!")
	}
	fmt.Println("AuthorID", m.Author)
	fmt.Println(m.ChannelID)
}
