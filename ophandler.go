package socbot

const (
	OpTextTemplate string = "here {{.Name}} MiniOP at {{.Time}} {{.Date}}\n```Mods:{{- range $i, $x := .Mods}}\n\t{{$x}}{{end}}\n\nInfo: {{.Info}}```"
)

type opCreate struct {
	Name string
	Time string
	Date string
	Mods []string
	Info string
}
